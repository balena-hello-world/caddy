# balena-hello-world
This [balena-hello-world](https://gitlab.com/balena-hello-world) -group implements an exemplary CI/CD -workflow for a [balenaCloud](https://www.balena.io/cloud/) -project, utilizing multiple containers.
Each container is maintained in it's own project, pushing Docker images to the [deployment registry](https://gitlab.com/balena-hello-world/deployment/container_registry).
[deployment](https://gitlab.com/balena-hello-world/deployment) -project is then used to push the whole project to [balenaCloud](https://www.balena.io/cloud/).

## balena-hello-world/caddy
This repository contains the [Caddy](https://caddyserver.com/) -part of [balena-hello-world](https://gitlab.com/balena-hello-world) -application.

[caddy](https://gitlab.com/balena-hello-world/caddy) -repository is responsible for serving the [flask](https://gitlab.com/balena-hello-world/flask) application via web server. This repository also takes care of creating it's own images and pushing them to the [deployment registry](https://gitlab.com/balena-hello-world/deployment/container_registry).
